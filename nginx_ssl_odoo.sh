#!/bin/bash

# Este script instala nginx y lo configura para trabajar con
# workers, redireccionando la salida del modulo im_chat que da un error
# en el log en la libreria bus.Bus, tambien el tema del longpolling odoo
#
#
# Default Odoo PORT, change if necessary
PORT=8069

# WEBSOCKET PORT ODOO
# Remember to add gevent_port = 8072  and proxy_mode = True parameters into odoo.conf
GEVENT_PORT=8072

echo "Ingrese el nombre de dominio para el servidor (ejemplo: tensor-analytics.com):"
read domain

echo "Ingrese un email (ejemplo: info@mail.com):"
read email

echo "************************************************"
echo "**********Actualizando repositorios...**********"
echo "************************************************"
echo "************************************************"
sudo apt-get update

sudo apt install snapd
sudo snap install core; sudo snap refresh core


################################################

# eliminando instalaciones certbot y nginx previas

sudo apt-get remove  --purge certbot nginx nginx-common
sudo snap remove --purge certbot

################################################


echo "*************** instalar configurar certificados **********************"

sudo apt-get -y install software-properties-common

sudo snap install --classic certbot
sudo ln -s /snap/bin/certbot /usr/bin/certbot
certbot certonly --standalone -d $domain,www.$domain -m $email --agree-tos -n

echo "************************************************"
echo "**********Instalando Nginx... ******************"
echo "************************************************"
echo "************************************************"
sudo service nginx stop
sudo apt-get install -y nginx

echo "************************************************"
echo "**********Configurando Nginx... ****************"
echo "************************************************"
echo "************************************************"

sudo rm /etc/nginx/sites-available/default
sudo rm /etc/nginx/sites-enabled/default
sudo rm /etc/nginx/sites-available/$domain
sudo rm /etc/nginx/sites-available/odoo
sudo rm /etc/nginx/sites-enable/$domain
sudo rm /etc/nginx/sites-enable/odoo

sudo touch /etc/nginx/sites-available/$domain
sudo ln -s /etc/nginx/sites-available/$domain /etc/nginx/sites-enabled/
sudo mkdir /etc/nginx/ssl
sudo openssl dhparam -out /etc/nginx/ssl/dhp-2048.pem 2048

echo "
upstream odoo {
    server 127.0.0.1:$PORT;
}

upstream odoochat {
    server 127.0.0.1:$GEVENT_PORT;
}

map $http_upgrade $connection_upgrade {
    default upgrade;
    ''      close;
}

# Activar esto cuando se use workers unicamente
upstream openerp-im {
    server 127.0.0.1:$GEVENT_PORT weight=1 fail_timeout=0;
}

server {
    listen 443 ssl http2;
    server_name www.$domain $domain;
    rewrite ^(.*) https://$host$1 permanent;
    
    if ($host = 'www.$domain') {
        return 301 https://$domain\$request_uri;
    }

    add_header X-Content-Type-Options nosniff;
    add_header X-Frame-Options SAMEORIGIN;
    add_header X-XSS-Protection "1; mode=block";
    add_header Strict-Transport-Security "max-age=31536000; includeSubDomains" always;
    add_header Referrer-Policy "strict-origin-when-cross-origin";

    client_body_buffer_size 16k;
    client_max_body_size 10m;
    client_header_buffer_size 1k;
    large_client_header_buffers 4 8k;

    proxy_buffers 16 64k;
    proxy_buffer_size 128k;
    proxy_read_timeout 720s;
    proxy_connect_timeout 720s;
    proxy_send_timeout 720s;

    access_log  /var/log/nginx/odoo.access.log;
    error_log   /var/log/nginx/odoo.error.log;

    ssl_certificate /etc/ssl/nginx/$domain.crt;
    ssl_certificate_key /etc/ssl/nginx/$domain.key;
    ssl_trusted_certificate /etc/ssl/nginx/$domain.ca-bundle;

    ssl_stapling on;
    ssl_stapling_verify on;
    resolver 8.8.8.8 8.8.4.4 valid=300s;

    ssl_session_timeout 30m;
    ssl_session_cache shared:SSL:10m;
    ssl_session_tickets off;

    keepalive_timeout   60;
    keepalive_requests 100000;
    client_body_timeout 12;
    client_header_timeout 12;
    send_timeout 10;
    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;

    ssl_ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384;
    ssl_protocols TLSv1.2 TLSv1.3;
    ssl_prefer_server_ciphers off;
    # ssl_dhparam /etc/nginx/ssl/dhp-2048.pem;

    location / {
        proxy_pass http://odoo;
        proxy_next_upstream error timeout invalid_header http_500 http_502 http_503 http_504;
        proxy_redirect off;

        proxy_set_header    X-Real-IP $remote_addr;
        proxy_set_header    X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header    X-Forwarded-Host  $http_host;
        proxy_set_header    X-Forwarded-Proto $scheme;

        add_header Strict-Transport-Security "max-age=31536000; includeSubDomains";
        proxy_cookie_flags session_id samesite=lax secure;
    }

    location ~* /web/static/ {
        proxy_cache_valid 200 365d;
        proxy_buffering on;
        expires 365d;
        proxy_pass http://odoo;
    }

    location ~* \.(eot|ttf|woff|woff2)$ {
        add_header Access-Control-Allow-Origin *;
    }

    # Activado para trabajar en Odoo v18 Web socket
    location /websocket {
        proxy_pass http://odoochat;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $connection_upgrade;
        proxy_set_header X-Forwarded-Host $http_host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Real-IP $remote_addr;

        add_header Strict-Transport-Security "max-age=31536000; includeSubDomains";
        proxy_cookie_flags session_id samesite=lax secure;
    }

    gzip on;
    gzip_disable msie6;
    gzip_vary on;
    gzip_proxied any;
    gzip_comp_level 6;
    gzip_buffers 16 8k;
    gzip_http_version 1.1;
    gzip_min_length 256;
    gzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript application/vnd.ms-fontobject application/x-font-ttf font/opentype font/woff image/svg+xml image/x-icon;

}

server {
    listen 80;
    server_name www.$domain $domain;
    listen [::]:80 ipv6only=on;

    add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload" always;
    return 301 https://$host$request_uri;
}" > /etc/nginx/sites-available/$domain

echo "***************************************************"
echo "**********Comprobando configuracion...*************"
echo "***************************************************"
echo "***************************************************"
sudo nginx -t

echo "***************************************************"
echo "********** Reiniciando servicios...****************"
echo "***************************************************"
echo "***************************************************"

# sudo /etc/init.d/odoo-server restart


# Fix error 400 uri too large
sudo sed -i '/large_client_header_buffers/d' /etc/nginx/nginx.conf
sudo sed -i 's/sendfile on;/large_client_header_buffers 4 32k;\n\tsendfile on;/' /etc/nginx/nginx.conf
sudo /etc/init.d/nginx restart


# ############## Crontab para renovar certificados SSL  ######################
# ################### todos los lunes en la madrugada ########################
sudo -u root bash << eof
whoami
cd /root
echo "Agregando crontab para renovar certificados SSL..."

sudo crontab -l | sed -e '/certbot/d; /nginx/d' > temporal

echo "
35 2 * * 1 /root/.renew_ssl.sh" >> temporal
crontab temporal
rm temporal
eof

sudo cp ./renew_ssl.sh /root/.renew_ssl.sh

echo "******************************************************************"
echo "**********Terminado***********************************************"
echo "***********Ya puede acceder a su instancia http://$domain*********"
echo "******************************************************************"